import React from 'react';

const Footer = ({url, text}) => (
	<div className="footer">
		<p>
			<a href={url}>{text}</a>
		</p>
	</div>
);

export default Footer;
