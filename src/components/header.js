import React from 'react';

const Header = ({title, subtitle}) => (
	<header className="header">
		<img src={require('../assets/uob-logo.svg')}
			alt='University of Bath Logo'/>
		<h1>{title}</h1>
		<h2>{subtitle}</h2>
	</header>
);

export default Header;
