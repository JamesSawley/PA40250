import React from "react";
import { Element } from "react-scroll";

const isEven = x => {
  return x % 2 === 0;
};

const paperStyle = (width, id) => {
  if (width > 900) {
    const height = document.getElementById(id).clientHeight;
    return { minHeight: `${height - 40}px` };
  }
  return {};
};

const Body = ({ fix, menuSize, weeks, intro, summary, references, width }) => (
  <div id="page-body" style={{ marginTop: fix ? menuSize : "0" }}>
    <div className="introduction odd-tile">
      {intro.map((text, index) => {
          return <p key={`intro-${index}`}>{text}</p>
      })}
    </div>
    {weeks.map((week, index) => (
      <Element
        key={`body-${week.title}`}
        name={week.title}
        className={"container " + (isEven(index) ? "even-tile" : "odd-tile")}>
        <div className="container-body">
          <div className="left" id={`left-${index + 1}`}>
            <div
              className="paper"
              style={paperStyle(width, `right-${index + 1}`)}>
              <h2>{week.title}</h2>
              <h3>{week.subtitle}</h3>
              {week.diary.text.map((text, index) => (
                <p key={"diary-entry" + index}>{text}</p>
              ))}
            </div>
          </div>
          <div className="right" id={`right-${index + 1}`}>
            <div className="story-board">
              <h1>{week.storyboard.header}</h1>
              <p>{week.storyboard.footer}</p>
              {week.storyboard.images.map((imgf, index) => (
                <img
                  src={require(`../assets/${imgf.fileName}`)}
                  alt={imgf.alt}
                  key={imgf.alt + index}
                />
              ))}
            </div>
          </div>
        </div>
      </Element>
    ))}
    <div
      className={
        "summary " + (isEven(weeks.length) ? "even-tile" : "odd-tile")
      }>
      <h2>Summary</h2>
      {summary.map((text, index) => {
          return <p key={`summary-${index}`}>{text}</p>
      })}
    </div>
    <div
      className={
        "references " + (isEven(weeks.length) ? "odd-tile" : "even-tile")
      }>
      <h2>References</h2>
      <ol>
        {references.map((text, index) => {
            return <li key={`reference-${index}`}>{text}</li>
        })}
      </ol>
    </div>
  </div>
);

export default Body;
