import React, {Component} from 'react';
import {Link} from 'react-scroll';

class Menu extends Component {
	constructor(props) {
		super(props);
		this.menuClosed = this.menuClosed.bind(this);
		this.toggleMenu = this.toggleMenu.bind(this);
		this.state = { menuChange: false };
	}
	menuClosed() {
		this.setState({ menuChange: false });
	}
	toggleMenu() {
		this.setState({ menuChange: !this.state.menuChange });
	}
	render() {
		return (
			<div className={"menu " + (this.props.fix ? "menu-fixed" : "menu-scroll")}>
				<div id="scroll-line" className="scroll-line"
					style={{"width": `${this.props.offset}%`}}>
				</div>
				<div id="menu" className={`menu-options ${this.state.menuChange ? "change": ""}`}>
					{this.props.width < 600
						&& <div className="burger-menu" onClick={this.toggleMenu}>
					  		<div className="burger-menu-component-1"></div>
					  		<div className="burger-menu-component-2"></div>
					  		<div className="burger-menu-component-3"></div>
						</div>}
					<div className={`${this.props.width < 600 ? "vertical": "horizonal"}`}>
						{
							this.props.config.weeks.map(week =>
								<Link
									activeClass="active-menu-option"
									className="menu-link"
									key={`menu-${week.title}`}
									to={week.title}
									onSetActive={()=>this.menuClosed()}
									spy={true}
									smooth={true}
									duration={500}
									offset={-this.props.menuSize}>
									{week.title}
								</Link>
							)
						}
					</div>
				</div>
			</div>
		);
	}
}

export default Menu;
