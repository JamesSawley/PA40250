import React from 'react';
import ReactDOM from 'react-dom';
import * as firebase from "firebase";
import App from './App';
import registerServiceWorker from './registerServiceWorker';

const config = {
		apiKey: "AIzaSyDzCVPLzbcNoF6GFOhh0bawFVhpy_DUYBQ",
		authDomain: "pa40250-sophie-wilmshurst.firebaseapp.com",
		databaseURL: "https://pa40250-sophie-wilmshurst.firebaseio.com",
		projectId: "pa40250-sophie-wilmshurst",
		storageBucket: "pa40250-sophie-wilmshurst.appspot.com",
		messagingSenderId: "139470320586"
	};
firebase.initializeApp(config);

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
