import React, { Component } from "react";
import "styles/_index.scss";
import config from "./config.json";
import Body from "components/body";
import Footer from "components/footer";
import Header from "components/header";
import Menu from "components/menu";

class App extends Component {
  constructor() {
    super();
    this.scrolled = this.scrolled.bind(this);
    this.windowWidth = this.windowWidth.bind(this);
    this.state = {
      fix: false,
      offset: 0
    };
  }
  componentDidMount() {
    window.addEventListener("scroll", this.scrolled);
    window.addEventListener("resize", this.windowWidth);
    this.windowWidth();
  }
  componentWillUnmount() {
    window.removeEventListener("scroll", this.scrolled);
    window.removeEventListener("resize", this.windowWidth);
  }
  scrolled() {
    const headerHeight = 100;
    const offset = window.pageYOffset;
    const docHeight = document.getElementById("root").scrollHeight;
    const windowHeight = window.innerHeight;
    if (offset > 100) {
      this.setState({
        fix: true,
        offset:
          100 *
          ((offset - headerHeight) / (docHeight - windowHeight - headerHeight))
      });
    } else {
      this.setState({ fix: false, offset: 0 });
    }
  }
  windowWidth() {
    const menuHeight = document.getElementById("menu").offsetHeight;
    if (window.innerWidth < 600) {
      this.setState({ menuSize: 45, width: window.innerWidth });
    } else {
      this.setState({ menuSize: menuHeight, width: window.innerWidth });
    }
  }

  render() {
    return (
      <div className="App">
        <Header title={config.header.title} subtitle={config.header.subtitle} />
        <Menu
          config={config}
          fix={this.state.fix}
          menuSize={this.state.menuSize}
          offset={this.state.offset}
          width={this.state.width}
        />
        <Body
          fix={this.state.fix}
          menuSize={this.state.menuSize}
          weeks={config.weeks}
          intro={config.introduction}
          summary={config.summary}
          references={config.references}
          width={this.state.width}
        />
        <Footer text={config.footer.text} url={config.footer.url} />
      </div>
    );
  }
}

export default App;
